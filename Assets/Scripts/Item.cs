using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public static Transform dragFrom;
    public static Item dragItem;

    [SerializeField] private Sound _clickSound;
    [SerializeField] private Image _image;

    private void Update()
    {
        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2))
        {
            if(dragItem != null)
            {
                Drop(dragFrom);
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _image.raycastTarget = false;
        dragFrom = transform.parent;
        transform.SetParent(_image.canvas.transform);
        dragItem = this;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _image.raycastTarget = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _clickSound.Play();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = Vector2.one * 1.5f;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = Vector2.one;
    }

    private void Drop(Transform parent)
    {
        transform.SetParent(parent);
        transform.localPosition = Vector3.zero;
        dragFrom = null;
        dragItem = null;
    }
}
