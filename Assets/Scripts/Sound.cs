using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _clip;

    public void Play()
    {
        if (!_audioSource.isPlaying)
        {
            _audioSource.clip = _clip;
            _audioSource.Play();
        }
    }
}
